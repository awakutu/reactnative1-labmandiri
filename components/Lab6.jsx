import React from 'react';
import {useState} from 'react';
import { Button, Text, View } from 'react-native';

 const Cat = (props) => {
  const [isHungry, setIshungry] = useState(true);
  return (
    <View>
      <Text>Nama saya {props.name}, dan saya {isHungry ? "lapar" : "kenyang"}!</Text>
     <Button 
     onPress={()=>{
       setIshungry(false);
     }}
     disabled={!isHungry}
     title={isHungry ? "Beri ikan" : "Terima kasih!"}
     />
    </View>
  );
}

export default function Cafe ()  {
  return(
    <>
    <Cat name="Kitty" />
    <Cat name="Meong" />
    </>
  )
}


