import React from "react";
import { Text, View, StyleSheet } from "react-native";
import { responsiveFontSize } from "react-native-responsive-dimensions";

export default function App() {
  return (
    <View style={styles.viewContainer}>
      <View style={styles.viewWrapper}>
        <Text style={styles.textBold}>Hello I'm Bold</Text>
        <Text style={styles.textItalic}>Hello I'm Italic</Text>
        <Text style={styles.textUnderline}>Hello I'm Underline</Text>
        <Text style={styles.textBig}>Hello I'm Big</Text>
        <Text style={styles.textBigColor}>Hello I'm Big and Have Color</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
  },
  viewWrapper: {
    alignItems: "center",
  },
  textBold: {
    fontSize: responsiveFontSize(2.2),
    fontWeight: "bold",
  },
  textItalic: {
    fontSize: responsiveFontSize(2.2),
    fontStyle: "italic",
  },
  textUnderline: {
    fontSize: responsiveFontSize(2.2),
    textDecorationLine: "underline",
  },
  textBig: {
    fontSize: responsiveFontSize(3.5),
    fontWeight: "bold",
  },
  textBigColor: {
    fontSize: responsiveFontSize(3.5),
    fontWeight: "bold",
    color: "blue",
  },
});
