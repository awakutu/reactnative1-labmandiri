import React, { useState } from "react";
import { Image, Text, TextInput, View, StyleSheet, TouchableOpacity} from "react-native";

export default function App() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  return (
    <View style={styles.viewContainer}>
      <View style={styles.viewWrapper}>
        <Image
          style={styles.logo}
          source={{
            uri: "https://codelatte.org/wp-content/uploads/2018/07/fixcil.png",
          }}
        />
        <Text style={styles.textBold}>LOGIN HERE</Text>
        <TextInput
          style={styles.input}
          onChange={(e) => setUsername(e.target.value)}
          placeholder={"Masukkan username"}
        />
        <TextInput
          secureTextEntry={true}
          style={styles.input}
          onChange={(e) => setPassword(e.target.value)}
          placeholder={"Masukkan password"}
        />
          <TouchableOpacity
               style = {styles.submitButton}
               >
               <Text style = {styles.submitButtonText}> Submit </Text>
            </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
  },
  viewWrapper: {
    alignItems: "center",
  },
  logo: {
    width: 100,
    height: 100,
  },
  input: {
    height: 27,
    width: 250,
    fontSize: 10,
    borderColor: "gray",
    borderWidth: 1,
    marginTop: 15,
    
  },
  textLogin: {
    color: "blue",
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40,
 },
 submitButtonText:{
    color: 'white'
 }
});
