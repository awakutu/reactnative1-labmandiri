import React from "react";
import { Button, View, Text } from "react-native";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

const HomeScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
    <Text>
      Home Screen
    </Text>
    <Button title="Go to details"
      onPress={()=> navigation.navigate('Details')}
    />
    </View>
  );
}

const DetailsScreen = ({navigation}) => {
  return(
    <View style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Details Screen</Text>
      <Button
      title="Go to Details... again"
      onPress={() => navigation.push('Details')}/>
      <Button title="Go to Home" onPress={() => navigation.navigate('Home')}/>
      <Button title="Go back" onPress={() => navigation.goBack()}/>
      <Button title="Go to first screen in stack" onPress={() => navigation.popToTop()}/>
    </View>
  )
}

export default function App() {
  return(
    <NavigationContainer>
    <Stack.Navigator initialRouteName= "Home">
      <Stack.Screen name="Home" component={HomeScreen}/>
      <Stack.Screen name="Details" component={DetailsScreen}/>
    </Stack.Navigator>
    </NavigationContainer>
  )
}