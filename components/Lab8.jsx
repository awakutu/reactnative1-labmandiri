import React from 'react';
import { StyleSheet, Text,  View } from "react-native";

export default function App ()  {
  return(
    <View style={tampilan.container}>
      <Text style={tampilan.textTitle}>Selamat Datang</Text>
      <Text style={tampilan.textTitle}>Di</Text>
      <Text style={tampilan.textTitle}>Aplikasi ABC</Text>
    </View>
  )
}


const tampilan = StyleSheet.create ({
  container: {
    backgroundColor: '#EBEBEB',
    alignItems: 'center',
  },
  textTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#43B2EC',
  },
})
